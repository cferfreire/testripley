// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDwESvbmayYPdIGGyBJmYyhe3vI8K5_rK0",
    authDomain: "testripley-e9847.firebaseapp.com",
    projectId: "testripley-e9847",
    storageBucket: "testripley-e9847.appspot.com",
    messagingSenderId: "1086668080123",
    appId: "1:1086668080123:web:17b0d8fc36b7b921fd815e",
    measurementId: "G-QZHBD04QCS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
