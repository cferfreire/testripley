import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormularioInsComponent } from './components/formulario-ins/formulario-ins.component';
import { TransferenciaComponent } from './components/transferencia/transferencia.component';
import { HistoTransferenciaComponent } from './components/histo-transferencia/histo-transferencia.component';
import { HomeComponentComponent } from './components/home-component/home-component.component';
import { WarningGuard } from './guards/warning.guard';

const routes: Routes = [
  { path: 'home-component', component: HomeComponentComponent},
  { path: 'form-component', component: FormularioInsComponent},
  { path: 'transac-component', component: TransferenciaComponent },
  { path: 'transacHist-component', component: HistoTransferenciaComponent },
  { path: '', redirectTo: '/home-component', pathMatch: 'full' },
  //{ path: '**', pathMatch:'full', component: NoFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
