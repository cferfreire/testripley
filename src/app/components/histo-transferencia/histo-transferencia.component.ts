import { Component, OnInit } from '@angular/core';
import { TransfService } from 'src/app/services/transf.service';

@Component({
  selector: 'app-histo-transferencia',
  templateUrl: './histo-transferencia.component.html',
  styleUrls: ['./histo-transferencia.component.scss'],
  providers : [TransfService]
})

export class HistoTransferenciaComponent implements OnInit {

  constructor(private dataTrf : TransfService ) { }
  contacto;
  contactos = []

  ngOnInit(): void {
    this.dataTrf.getTransfes().subscribe( contacto => {
      this.contactos = contacto ;
    })

}

}
