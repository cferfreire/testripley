import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoTransferenciaComponent } from './histo-transferencia.component';

describe('HistoTransferenciaComponent', () => {
  let component: HistoTransferenciaComponent;
  let fixture: ComponentFixture<HistoTransferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoTransferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoTransferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
