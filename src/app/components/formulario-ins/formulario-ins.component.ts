import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { ServicesService } from 'src/app/services/services.service'
import Swal from 'sweetalert2';
import { rutValidate, rutFormat } from 'rut-helpers';

@Component({
  selector: 'app-formulario-ins',
  templateUrl: './formulario-ins.component.html',
  styleUrls: ['./formulario-ins.component.scss'],
  providers: [DataService]
})
export class FormularioInsComponent implements OnInit {

  formulario: FormGroup;
  bancos;
  config: any;
  listaCuentas : string[]=["Cuenta Corriente","Cuenta Vista","Cuenta Ahorro"];

  constructor(private data: ServicesService, private dataSvc : DataService ) {
    
    this.formulario = new FormGroup({
      'nombre': new FormControl('', [Validators.required] ),
      'rut': new FormControl('',[
        Validators.required]),
      'correo': new FormControl('', [
        Validators.required,
        Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-z]{2,3}$')
      ]),
      'numTel': new FormControl('', 
      [Validators.required,
        Validators.pattern('[0-9]{1,10}')]),
      'tipCta': new FormControl('', 
      [Validators.required]),
      'numCta': new FormControl('', 
      [Validators.required,
        Validators.pattern('[0-9]{1,15}')]),
      'bancoList': new FormControl('', 
      [Validators.required]),
    });
  }

    async miSubmit(): Promise<void> {
     
      if (this.formulario.valid)
      {
        console.log(this.formulario.value );
        try {
          await this.dataSvc.onSaveContacts(this.formulario.value);
          Swal.fire({
            icon: 'success',
            title: 'Dato ingresado Exitosamente',
            text: 'Presione OK para continuar...',
          })
          this.formulario.reset();

        } catch(error){
        
          Swal.fire({
            icon: 'error',
            title: 'Opps!!.',
            text: 'Ha ocurrido un error ... :(',
          })
        
        }
      }
         
    }
      miBorrar(): void{
        this.formulario.reset();

      }
        ngOnInit(): void {

          this.data.getBanks().subscribe(data => {
            this.bancos = data ;
          })
      }
}