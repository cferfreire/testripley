import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioInsComponent } from './formulario-ins.component';

describe('FormularioInsComponent', () => {
  let component: FormularioInsComponent;
  let fixture: ComponentFixture<FormularioInsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioInsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioInsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
