import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { TransfService } from 'src/app/services/transf.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.scss'],
  providers: [DataService, TransfService]
})
export class TransferenciaComponent implements OnInit {
  
  formulario: FormGroup;

  
  constructor(private dataSvc : DataService, private dataTrf : TransfService  ) {
    
    this.formulario = new FormGroup({

      'nombre': new FormControl('',[Validators.required]),
      'rut': new FormControl('',[Validators.required]),
      'correo': new FormControl('',[Validators.required]),
      'numTel': new FormControl('',[Validators.required]),
      'tipCta': new FormControl('',[Validators.required]),
      'numCta': new FormControl('',[Validators.required]),
      'bancoList': new FormControl('',[Validators.required]),
      'monto': new FormControl('', [
        Validators.required,
        
        Validators.pattern('^[1-9][0-9]*$')] ),
      'destino' : new FormControl('')
        });
   }
  


  contacto;
  contactos = []
  
  filterDest = ''


  miSelect(contacto): void{

  this.formulario.controls['nombre'].setValue(contacto.nombre);
  this.formulario.controls['rut'].setValue(contacto.rut);
  this.formulario.controls['correo'].setValue(contacto.correo);
  this.formulario.controls['numTel'].setValue(contacto.numTel);
  this.formulario.controls['tipCta'].setValue(contacto.tipCta);
  this.formulario.controls['numCta'].setValue(contacto.numCta);
  this.formulario.controls['bancoList'].setValue(contacto.bancoList);
 
  }

  async miSubmit(): Promise<void> {
     
    if (this.formulario.valid)
    {
      console.log(this.formulario.value );
      try {
        await this.dataTrf.onSaveTransfer(this.formulario.value);
        Swal.fire({
          icon: 'success',
          title: 'Transferencia Realizada Exitosamente',
          text: 'Presione OK para continuar...',
        })
        this.formulario.reset();

      } catch(error){
      
        Swal.fire({
          icon: 'error',
          title: 'Opps!!.',
          text: 'Ha ocurrido un error ... :(',
        })
      
      }
    }
  }

  miBorrar(){
    this.formulario.reset();
  }

  ngOnInit(): void {
    this.dataSvc.getContacts().subscribe( contacto => {
      this.contactos = contacto ;
    })

    
  }

}
