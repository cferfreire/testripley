import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { FormularioInsComponent } from '../components/formulario-ins/formulario-ins.component';


export class WarningGuard implements CanDeactivate<FormularioInsComponent> {
    canDeactivate(component:FormularioInsComponent ): Observable<boolean> | Promise<boolean> | boolean {

    if (component.formulario.dirty)
    {
        Swal.fire({
            title: 'Está seguro de salir sin ingresar datos?',
            text: "se perderan los datos no guardados.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok'
          }).then((result) => {
            if (result.isConfirmed) {
              return result.isConfirmed ? true : false;
            }
          });

        return true;
        
    }
        
    }

}