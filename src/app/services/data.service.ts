import { StringMap } from "@angular/compiler/src/compiler_facade_interface";
import { Injectable } from "@angular/core";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from "rxjs";

export interface Contactos {
    nombre : string;
    rut : string;
    email : string;
    telefono : string; 
    banco : string;
    cuenta : string;
    numcta : string;

}

@Injectable()
export class DataService {
    contacts: Observable<Contactos[]>;
    private contactsCollection: AngularFirestoreCollection<Contactos>;

    constructor(private readonly afs: AngularFirestore){
    this.contactsCollection = afs.collection<Contactos>('destinatarios')
    this.contacts = this.contactsCollection.valueChanges();

}
    getContacts(){
        return this.contacts

    }

    async onSaveContacts (formulario:Contactos): Promise<void>{
        return new Promise (async (resolve, reject) => {
            try {
                const id = this.afs.createId();
                const data = {id,... formulario}
                const result = this.contactsCollection.doc(id).set(data);
                resolve(result)
            }catch (error){
                reject (error.message)
            }
        
        }
     
        
     )

     
    }



}