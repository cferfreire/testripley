import { StringMap } from "@angular/compiler/src/compiler_facade_interface";
import { Injectable } from "@angular/core";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from "rxjs";

export interface Transferencia {
    nombre : string;
    rut : string;
    email : string;
    telefono : string; 
    banco : string;
    cuenta : string;
    numcta : string;
    monto : string;

}

@Injectable()
export class TransfService {
    contacts: Observable<Transferencia[]>;
    private contactsCollection: AngularFirestoreCollection<Transferencia>;

    constructor(private readonly afs: AngularFirestore){
    this.contactsCollection = afs.collection<Transferencia>('transferencias')
    this.contacts = this.contactsCollection.valueChanges();

}
    getTransfes(){
        return this.contacts

    }

    async onSaveTransfer (formulario:Transferencia): Promise<void>{
        return new Promise (async (resolve, reject) => {
            try {
                const id = this.afs.createId();
                const data = {id,... formulario}
                const result = this.contactsCollection.doc(id).set(data);
                resolve(result)
            }catch (error){
                reject (error.message)
            }
        
        }
     
        
     )

     
    }



}