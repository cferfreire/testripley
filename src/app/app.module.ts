import {ReactiveFormsModule} from '@angular/forms';
import { FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormularioInsComponent } from './components/formulario-ins/formulario-ins.component';
import { TransferenciaComponent } from './components/transferencia/transferencia.component';
import { HistoTransferenciaComponent } from './components/histo-transferencia/histo-transferencia.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HomeComponentComponent } from './components/home-component/home-component.component';
import { HttpClientModule} from '@angular/common/http';
import { ServicesService } from './services/services.service';
import {AngularFirestore} from '@angular/fire/firestore'
import {AngularFireModule} from '@angular/fire'
import { environment } from 'src/environments/environment';
import { WarningGuard } from './guards/warning.guard';
import { PipesPipe } from './pipes/pipes.pipe';


@NgModule({
  declarations: [
    AppComponent,
    FormularioInsComponent,
    TransferenciaComponent,
    HistoTransferenciaComponent,
    NavBarComponent,
    HomeComponentComponent,
    PipesPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
  ],
  providers: [AngularFirestore, WarningGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
